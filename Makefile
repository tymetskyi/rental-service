SHELL := /bin/bash -e

help:
	@echo 'Whole app commands:'
	@echo 'make start                           Start and build all containers'
	@echo 'make down                            Down all containers'
	@echo 'make stop                            Stop all images'
	@echo 'make restart                         Restart all images'
	@echo 'make clean-force                     Cleans all volumes'
	@echo 'make rm                              Remove all containers and images'
	@echo
	@echo 'make migrate                         Migrate DB. Given name is optional.'
	@echo 'make showmigrations                  Show migration tables states.'
	@echo 'make makemigrations                  Create new migrations. Given name is optional'
	@echo
	@echo 'make check linters                   Check linters'
	@echo 'make run_linters                     Run linters'
	@echo 'make test                            Run tests in inside container'
	@echo 'make all                             Run linters & tests'


start:
	$(info Make: Starting container.)
	@docker compose up --build

down:
	@docker compose down

stop:
	$(info Make: Stopping environment container.)
	@docker compose stop

restart:
	$(info Make: Restarting environment container.)
	@make -s stop
	@make -s start

clean-force:
	$(info Make: Force delete of docker container.)
	@docker system prune --volumes --force

rm:
	@docker compose stop $(docker ps -aq)
	@docker compose rm -f $(docker ps -aq)


migrate:
	$(info Make: Migrate DB with existing migrations; "name" args is optional.)
	@docker compose exec app python manage.py migrate $(name)

showmigrations:
	$(info Make: Migrate DB with existing migrations; "name" args is optional.)
	@docker compose exec app python manage.py showmigrations $(name)

makemigrations:
	$(info Make: Create new migration with given name.)
	@docker compose exec app python manage.py makemigrations

check_linters:
	interrogate
	black --check --diff .
	isort . -c -v
	flake8

run_linters:
	black .
	isort .

test:
	@docker compose exec app pytest . -vv

all: run_linters test
