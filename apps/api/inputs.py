import graphene

from api.enums import FlatRoomEnum, FlatTypeEnum, UserStatusEnum
from api.scalars import PositiveInt


class UserInput(graphene.InputObjectType):
    """User custom input."""

    first_name = graphene.String()
    last_name = graphene.String()
    status = UserStatusEnum()
    birthday = graphene.Date()
    phone_number = graphene.String()
    address = graphene.String()


class LocationInput(graphene.InputObjectType):
    """Location input."""

    country = graphene.String(required=True)
    city = graphene.String(required=True)


class FlatRoomAttributeValueInput(graphene.InputObjectType):
    """FlatRoomAttributeValue input."""

    attr_id = graphene.ID(required=True)
    count = PositiveInt(required=True)
    description = graphene.String()


class FlatRoomCreateInput(graphene.InputObjectType):
    """FlatRoom create input."""

    type = FlatRoomEnum(required=True)
    description = graphene.String()
    attributes = graphene.List(FlatRoomAttributeValueInput)


class FlatRoomUpdateInput(FlatRoomCreateInput):
    """FlatRoom update input."""

    id = graphene.ID(required=True)


class FlatCreateInput(graphene.InputObjectType):
    """Flat create input."""

    location = LocationInput(required=True)
    room_count = PositiveInt(required=True)
    type = FlatTypeEnum()
    price = graphene.Float()
    rooms = graphene.List(FlatRoomCreateInput, required=True)


class FlatUpdateInput(graphene.InputObjectType):
    """Flat update input."""

    location = LocationInput(required=True)
    room_count = PositiveInt(required=True)
    type = FlatTypeEnum()
    price = graphene.Float()
    rooms = graphene.List(FlatRoomUpdateInput, required=True)
