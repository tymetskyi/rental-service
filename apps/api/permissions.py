from graphql_jwt.decorators import user_passes_test
from graphql_jwt.exceptions import PermissionDenied

is_owner = user_passes_test(lambda u: u.is_owner)
is_renter = user_passes_test(lambda u: not u.is_owner)
login_owner_required = user_passes_test(lambda u: u.is_authenticated and u.is_owner)


def login_required_queryset(func):
    """Check login permission for qs."""

    def wrapper(cls, queryset, info, *args, **kwargs):
        if info.context.user.is_anonymous:
            raise PermissionDenied
        return func(cls, queryset, info, *args, **kwargs)

    return wrapper
