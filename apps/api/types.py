import graphene
import graphene_django_optimizer as gql_optimizer
from django_countries.graphql.types import Country
from graphene import relay
from graphene_django import DjangoObjectType

from api.permissions import login_required_queryset
from api.utils import get_role_for_query
from flats.models import Flat, FlatRoom, FlatRoomAttribute, FlatRoomAttributeValue
from locations.models import Location
from rent.models import MonthlyPayment, RentContract
from users.models import Owner, Renter, User


class UserType(DjangoObjectType):
    """User object type."""

    is_owner = graphene.Boolean()
    full_name = graphene.String()

    class Meta:
        model = User
        exclude = (
            "password",
            "is_staff",
            "is_superuser",
        )
        filter_fields = {
            "first_name": ["icontains"],
            "last_name": ["icontains"],
        }
        interfaces = (relay.Node,)

    @classmethod
    @login_required_queryset
    def get_queryset(cls, queryset, info):
        role = get_role_for_query(info.context.user.is_owner)
        qs = queryset.filter(role=role)
        return gql_optimizer.query(qs, info)  # optimized query


class OwnerType(DjangoObjectType):
    """Owner object type based on proxy model."""

    is_owner = graphene.Boolean()
    full_name = graphene.String()

    class Meta:
        model = Owner


class RenterType(DjangoObjectType):
    """Renter object type based on proxy model."""

    is_owner = graphene.Boolean()
    full_name = graphene.String()

    class Meta:
        model = Renter


class RentContractType(DjangoObjectType):
    """RentContract object type."""

    class Meta:
        model = RentContract
        exclude = ("renter",)


class MonthlyPaymentType(DjangoObjectType):
    """MonthlyPayment object type."""

    class Meta:
        model = MonthlyPayment
        exclude = ("renter_contract",)


class LocationType(DjangoObjectType):
    """Location object type."""

    country = graphene.Field(Country)

    class Meta:
        model = Location
        fields = (
            "id",
            "country",
            "city",
        )


class FlatType(DjangoObjectType):
    """Flat object type."""

    class Meta:
        model = Flat
        exclude = ("owner",)


class FlatRoomType(DjangoObjectType):
    """FlatRoom object type."""

    class Meta:
        model = FlatRoom
        exclude = (
            "flat",
            "attributes",
        )


class FlatAttributeType(DjangoObjectType):
    """FlatAttribute object type."""

    class Meta:
        model = FlatRoomAttribute
        exclude = (
            "flat_rooms",
            "attribute_values",
        )


class FlatAttributeValueType(DjangoObjectType):
    """FlatAttributeValue object type."""

    class Meta:
        model = FlatRoomAttributeValue
        exclude = ("flat_room",)


class UpdateFlatFail(graphene.ObjectType):
    """Object type for output after mutation."""

    error_message = graphene.String(required=True)
    ok = graphene.Boolean(required=True)


class UpdateFlatSuccess(graphene.ObjectType):
    """Object type for output after mutation."""

    flat = graphene.Field(FlatType, required=True)
    ok = graphene.Boolean(required=True)


class UpdateFlatPayload(graphene.Union):
    """Union type for output after mutation."""

    class Meta:
        types = (UpdateFlatFail, UpdateFlatSuccess)


class DeleteFlatFail(graphene.ObjectType):
    """Object type for output after mutation."""

    error_message = graphene.String(required=True)
    ok = graphene.Boolean(required=True)


class DeleteFlatSuccess(graphene.ObjectType):
    """Object type for output after mutation."""

    ok = graphene.Boolean(required=True)


class DeleteFlatPayload(graphene.Union):
    """Union type for output after mutation."""

    class Meta:
        types = (DeleteFlatFail, DeleteFlatSuccess)
