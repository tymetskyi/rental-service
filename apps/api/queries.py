import graphene
import graphene_django_optimizer as gql_optimizer
from graphene import relay
from graphene_django.filter import DjangoFilterConnectionField
from graphql_jwt.decorators import login_required

from api.types import UserType
from users.models import User


class Query(graphene.ObjectType):
    users = DjangoFilterConnectionField(UserType)
    user = relay.Node.Field(UserType)
    me = graphene.Field(UserType)

    @login_required
    def resolve_me(self, info):
        """Get authenticated user."""
        uid = info.context.user.id
        qs = User.objects.filter(id=uid)
        return gql_optimizer.query(qs, info).get()
