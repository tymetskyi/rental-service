from django.utils.translation import gettext_lazy as _

import graphene
from graphene_django.forms.mutation import DjangoModelFormMutation
from graphql_jwt.decorators import login_required

from api.inputs import FlatCreateInput, FlatUpdateInput, UserInput
from api.permissions import login_owner_required
from api.services import create_flat, update_flat
from api.types import (
    DeleteFlatFail,
    DeleteFlatPayload,
    DeleteFlatSuccess,
    FlatType,
    UpdateFlatFail,
    UpdateFlatPayload,
    UpdateFlatSuccess,
    UserType,
)
from flats.models import Flat
from users.forms import OwnerForm, RenterForm


class CreateOwner(DjangoModelFormMutation):
    """Create owner mutation."""

    class Meta:
        form_class = OwnerForm


class CreateRenter(DjangoModelFormMutation):
    """Create Renter mutation."""

    class Meta:
        form_class = RenterForm


class UpdateUser(graphene.Mutation):
    """Update user mutation."""

    class Arguments:
        data = UserInput(required=True)

    user = graphene.Field(UserType)
    ok = graphene.Boolean()

    @staticmethod
    @login_required
    def mutate(root, info, data=None, **kwargs):
        user = info.context.user
        for key, value in data.items():
            setattr(user, key, value)
        user.save()

        return UpdateUser(user=user, ok=True)


class DeleteUser(graphene.Mutation):
    """Delete user mutation."""

    user = graphene.Field(UserType)
    ok = graphene.Boolean()

    @staticmethod
    @login_required
    def mutate(root, info, data=None, **kwargs):
        user = info.context.user
        user.is_active = False
        user.save(update_fields=["is_active"])
        return UpdateUser(user=user, ok=True)


class CreateFlat(graphene.Mutation):
    """Create flat mutation."""

    flat = graphene.Field(FlatType)
    ok = graphene.Boolean()

    class Arguments:
        data = FlatCreateInput(required=True)

    @staticmethod
    @login_owner_required
    def mutate(root, info, data, **kwargs):
        flat = create_flat(info, data)
        return CreateFlat(flat=flat, ok=True)


class UpdateFlat(graphene.Mutation):
    """Update flat mutation."""

    class Arguments:
        flat_id = graphene.ID(required=True)
        data = FlatUpdateInput(required=True)

    Output = UpdateFlatPayload

    @staticmethod
    @login_owner_required
    def mutate(root, info, flat_id, data, **kwargs):
        user = info.context.user

        if not user.flats.filter(id=flat_id).exists():
            return UpdateFlatFail(
                error_message=_("User {} does not own an flat").format(user.id),
                ok=False,
            )

        flat = update_flat(data, flat_id)
        return UpdateFlatSuccess(flat=flat, ok=True)


class DeleteFlat(graphene.Mutation):
    """Delete flat mutation."""

    class Arguments:
        id = graphene.ID(required=True)

    Output = DeleteFlatPayload

    @staticmethod
    @login_owner_required
    def mutate(root, info, id, **kwargs):
        user = info.context.user

        try:
            flat = Flat.objects.get(id=id, owner=user)
        except Flat.DoesNotExist:
            return DeleteFlatFail(
                error_message=_("Flat with id {} and owner {} does not exists.").format(
                    id, user.id
                ),
                ok=False,
            )

        else:
            flat.delete()

        return DeleteFlatSuccess(ok=True)


class Mutation(graphene.ObjectType):
    create_owner = CreateOwner.Field()
    create_renter = CreateRenter.Field()
    update_user = UpdateUser.Field()
    delete_user = DeleteUser.Field()
    create_flat = CreateFlat.Field()
    update_flat = UpdateFlat.Field()
    delete_flat = DeleteFlat.Field()
