import pytest
from graphene_django.utils.testing import graphql_query
from graphql_jwt.settings import jwt_settings
from graphql_jwt.shortcuts import get_token
from graphql_relay import to_global_id
from model_bakery import baker

from flats.constants import TypeOfFlat, TypeOfFlatRoom


@pytest.fixture
def client_query(client):
    def func(*args, **kwargs):
        return graphql_query(*args, **kwargs, client=client)

    return func


@pytest.fixture
def user():
    user = baker.make("users.User", email="test@gmaoil.com")
    return user


@pytest.fixture
def user_with_global_id(user):
    user_global_id = to_global_id("UserType", user.id)
    return user, user_global_id


@pytest.fixture
def renter():
    renter = baker.make(
        "users.Renter", first_name="User", last_name="Renter", email="renter@gmaoil.com"
    )
    return renter


@pytest.fixture
def renter_with_global_id(renter):
    renter_global_id = to_global_id("UserType", renter.id)
    return renter, renter_global_id


@pytest.fixture
def owner():
    return baker.make("users.Owner", first_name="User", last_name="Owner", email="owner@gmail.com")


@pytest.fixture
def renters():
    return baker.make("users.Renter", _quantity=3)


@pytest.fixture
def owners():
    return baker.make("users.Owner", _quantity=3)


@pytest.fixture
def flat_attribute_chair():
    return baker.make("flats.FlatRoomAttribute", name="Chair")


@pytest.fixture
def flat_attribute_table():
    return baker.make("flats.FlatRoomAttribute", name="Table")


@pytest.fixture
def flat_attribute_bad():
    return baker.make("flats.FlatRoomAttribute", name="Double bad")


@pytest.fixture()
def flat_with_relations(owner, renter, flat_attribute_bad, flat_attribute_chair):
    flat = baker.make(
        "flats.Flat",
        owner=owner,
        price=200.2,
        type=TypeOfFlat.STUDIO,
        location__country="UA",
        location__city="Lviv",
    )

    flat_room1 = baker.make(
        "flats.FlatRoom",
        flat=flat,
        type=TypeOfFlatRoom.KITCHEN,
        description="Description",
    )
    flat_room2 = baker.make(
        "flats.FlatRoom",
        flat=flat,
        type=TypeOfFlatRoom.GUEST_ROOM,
        description="Description",
    )

    baker.make(
        "flats.FlatRoomAttributeValue",
        flat_attribute=flat_attribute_chair,
        flat_room=flat_room1,
        count=2,
        description="Description",
    )
    baker.make(
        "flats.FlatRoomAttributeValue",
        flat_attribute=flat_attribute_bad,
        flat_room=flat_room2,
        count=1,
        description="Description",
    )
    contract = baker.make("rent.RentContract", flat=flat, renter=renter, description="Description")
    baker.make(
        "rent.MonthlyPayment",
        renter_contract=contract,
        is_payed=True,
        date="2020-01-01",
    )
    return flat


def user_credentials(user):
    credentials = {
        jwt_settings.JWT_AUTH_HEADER_NAME: (
            f"{jwt_settings.JWT_AUTH_HEADER_PREFIX} {get_token(user)}"
        )
    }
    return credentials
