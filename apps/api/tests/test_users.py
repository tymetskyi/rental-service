import json
import uuid

import pytest

from api.tests.conftest import user_credentials
from users.constants import UserStatus
from users.models import Owner, Renter


@pytest.mark.django_db
class TestUser:
    def test_get_me_success(self, client_query, user_with_global_id):
        user, user_global_id = user_with_global_id
        credentials = user_credentials(user)

        response = client_query(
            """
            query {
                me {
                    id
                    email
                }
            }
            """,
            headers=credentials,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content
        assert content["data"] == {"me": {"id": user_global_id, "email": "test@gmaoil.com"}}

    def test_get_me_non_authenticated_should_fail(self, client_query):
        response = client_query(
            """
            query {
                me {
                    email
                }
            }
            """
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" in content
        assert (
            content["errors"][0]["message"] == "You do not have permission to perform this action"
        )

    def test_get_owners_list_success(self, client_query, renter, flat_with_relations):
        credentials = user_credentials(renter)
        response = client_query(
            """
            query {
                users {
                    edges {
                        node {
                            email
                            role
                            isOwner
                            flats {
                                type
                                price
                                location {
                                    country {
                                        name
                                        code
                                    }
                                    city
                                }
                                rooms {
                                    type
                                    description
                                    attributeValues {
                                        flatAttribute {
                                            name
                                        }
                                        count
                                        description
                                    }
                                }
                                rentContracts {
                                    description
                                    monthlyPayments {
                                        isPayed
                                        date
                                    }
                                }
                            }
                        }
                    }
                }
            }
            """,
            headers=credentials,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content
        assert content["data"] == {
            "users": {
                "edges": [
                    {
                        "node": {
                            "email": "owner@gmail.com",
                            "role": "OWNER",
                            "isOwner": True,
                            "flats": [
                                {
                                    "type": "STUDIO",
                                    "price": 200.2,
                                    "location": {
                                        "country": {"code": "UA", "name": "Ukraine"},
                                        "city": "Lviv",
                                    },
                                    "rooms": [
                                        {
                                            "type": "KITCHEN",
                                            "description": "Description",
                                            "attributeValues": [
                                                {
                                                    "flatAttribute": {"name": "Chair"},
                                                    "count": 2,
                                                    "description": "Description",
                                                }
                                            ],
                                        },
                                        {
                                            "type": "GUEST_ROOM",
                                            "description": "Description",
                                            "attributeValues": [
                                                {
                                                    "flatAttribute": {"name": "Double bad"},
                                                    "count": 1,
                                                    "description": "Description",
                                                }
                                            ],
                                        },
                                    ],
                                    "rentContracts": [
                                        {
                                            "description": "Description",
                                            "monthlyPayments": [
                                                {"isPayed": True, "date": "2020-01-01"}
                                            ],
                                        }
                                    ],
                                }
                            ],
                        }
                    }
                ]
            }
        }

    def test_get_renters_list_success(self, client_query, owner, flat_with_relations):
        credentials = user_credentials(owner)
        response = client_query(
            """
            query{
                users{
                    edges{
                        node{
                            email
                            role
                            isOwner
                            contracts {
                                description
                                monthlyPayments{
                                    date
                                    isPayed
                                }
                                flat{
                                    type
                                    price
                                    location {
                                        country {
                                            name
                                            code
                                        }
                                        city
                                    }
                                    rooms {
                                        type
                                        description
                                        attributeValues{
                                            flatAttribute{
                                                name
                                            }
                                            count
                                            description
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            """,
            headers=credentials,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content
        assert content["data"] == {
            "users": {
                "edges": [
                    {
                        "node": {
                            "email": "renter@gmaoil.com",
                            "role": "RENTER",
                            "isOwner": False,
                            "contracts": [
                                {
                                    "description": "Description",
                                    "monthlyPayments": [{"isPayed": True, "date": "2020-01-01"}],
                                    "flat": {
                                        "type": "STUDIO",
                                        "price": 200.2,
                                        "location": {
                                            "country": {
                                                "code": "UA",
                                                "name": "Ukraine",
                                            },
                                            "city": "Lviv",
                                        },
                                        "rooms": [
                                            {
                                                "type": "KITCHEN",
                                                "description": "Description",
                                                "attributeValues": [
                                                    {
                                                        "flatAttribute": {"name": "Chair"},
                                                        "count": 2,
                                                        "description": "Description",
                                                    }
                                                ],
                                            },
                                            {
                                                "type": "GUEST_ROOM",
                                                "description": "Description",
                                                "attributeValues": [
                                                    {
                                                        "flatAttribute": {"name": "Double bad"},
                                                        "count": 1,
                                                        "description": "Description",
                                                    }
                                                ],
                                            },
                                        ],
                                    },
                                }
                            ],
                        }
                    }
                ]
            }
        }

    def test_get_users_list_non_authenticated_should_fail(self, client_query, owners, renters):
        response = client_query(
            """
            query {
                users {
                    edges {
                        node {
                            role
                            isOwner
                        }
                    }
                }
            }
            """
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" in content
        assert (
            content["errors"][0]["message"] == "You do not have permission to perform this action"
        )

    def test_get_user_detail_success(self, client_query, renter_with_global_id, owner):
        _, renter_global_id = renter_with_global_id
        credentials = user_credentials(owner)

        response = client_query(
            """
            query user($id: ID!) {
                user (id: $id) {
                    id
                    email
                    fullName
                    role
                }
            }
            """,
            headers=credentials,
            variables={"id": renter_global_id},
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content
        assert content["data"] == {
            "user": {
                "id": renter_global_id,
                "email": "renter@gmaoil.com",
                "fullName": "User Renter",
                "role": "RENTER",
            }
        }

    def test_create_owner_success(self, client_query):
        email = f"{uuid.uuid4()}@gmail.com"
        response = client_query(
            """
            mutation createOwner($input: CreateOwnerInput!) {
                createOwner(input: $input) {
                    owner {
                        email
                        fullName
                        address
                    }
                }
            }
            """,
            input_data={
                "firstName": "Jon",
                "lastName": "Owner",
                "password1": "bfjndkml12*",
                "password2": "bfjndkml12*",
                "email": email,
                "address": "Ivana Franka, 33",
            },
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content

        is_owner_exists = Owner.objects.filter(email=email).exists()
        assert is_owner_exists
        assert content["data"] == {
            "createOwner": {
                "owner": {
                    "email": email,
                    "fullName": "Jon Owner",
                    "address": "Ivana Franka, 33",
                }
            }
        }

    def test_create_renter_success(self, client_query):
        email = f"{uuid.uuid4()}@gmail.com"
        response = client_query(
            """
            mutation createRenter($input: CreateRenterInput!) {
                createRenter(input: $input) {
                    renter {
                        email
                        fullName
                        address
                    }
                }
            }
            """,
            input_data={
                "firstName": "Jon",
                "lastName": "Renter",
                "password1": "bfjndkml12*",
                "password2": "bfjndkml12*",
                "email": email,
                "address": "Ivana Franka, 33",
            },
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content

        is_owner_exists = Renter.objects.filter(email=email).exists()
        assert is_owner_exists
        assert content["data"] == {
            "createRenter": {
                "renter": {
                    "email": email,
                    "fullName": "Jon Renter",
                    "address": "Ivana Franka, 33",
                }
            }
        }

    def test_update_me_success(self, client_query, renter_with_global_id):
        renter, renter_global_id = renter_with_global_id
        credentials = user_credentials(renter)

        response = client_query(
            """
            mutation updateUser($input: UserInput!) {
                updateUser(data: $input) {
                    ok
                    user {
                        id
                        firstName
                        lastName
                        birthday
                        phoneNumber
                        status
                        address
                    }
                }
            }
            """,
            headers=credentials,
            input_data={
                "firstName": "Jon",
                "lastName": "Renter",
                "status": "INACTIVE",
                "birthday": "2020-01-02",
                "phoneNumber": "+38000000000",
                "address": "Ivana Franka, 33",
            },
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content

        is_renter_updated = Renter.objects.filter(
            first_name="Jon",
            last_name="Renter",
            status=UserStatus.INACTIVE,
            birthday="2020-01-02",
            phone_number="+38000000000",
            address="Ivana Franka, 33",
        ).exists()
        assert is_renter_updated
        assert content["data"] == {
            "updateUser": {
                "ok": True,
                "user": {
                    "id": renter_global_id,
                    "firstName": "Jon",
                    "lastName": "Renter",
                    "status": "INACTIVE",
                    "birthday": "2020-01-02",
                    "phoneNumber": "+38000000000",
                    "address": "Ivana Franka, 33",
                },
            }
        }

    def test_delete_me_success(self, client_query, renter_with_global_id):
        renter, renter_global_id = renter_with_global_id
        credentials = user_credentials(renter)

        assert renter.is_active

        response = client_query(
            """
            mutation deleteUser {
                deleteUser {
                    user {
                        id
                        isActive
                    }
                    ok
                }
            }
            """,
            headers=credentials,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content

        renter.refresh_from_db()
        assert not renter.is_active
        assert content["data"] == {
            "deleteUser": {
                "user": {"id": renter_global_id, "isActive": False},
                "ok": True,
            }
        }
