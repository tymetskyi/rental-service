import copy
import json

import pytest
from model_bakery import baker

from api.tests.conftest import user_credentials
from flats.constants import TypeOfFlat, TypeOfFlatRoom
from flats.models import Flat, FlatRoomAttributeValue


@pytest.fixture
def input_create_data(flat_attribute_chair, flat_attribute_table, flat_attribute_bad):
    data = {
        "type": "HOUSE",
        "price": 400.2,
        "roomCount": 3,
        "location": {"country": "US", "city": "New York"},
        "rooms": [
            {
                "type": "KITCHEN",
                "description": "Description",
                "attributes": [
                    {
                        "attrId": flat_attribute_chair.id,
                        "count": 2,
                        "description": "Description",
                    },
                    {
                        "attrId": flat_attribute_table.id,
                        "count": 2,
                        "description": "Description",
                    },
                ],
            },
            {
                "type": "GUEST_ROOM",
                "description": "Description",
                "attributes": [
                    {
                        "attrId": flat_attribute_bad.id,
                        "count": 1,
                        "description": "Description",
                    },
                ],
            },
        ],
    }
    return data


@pytest.fixture
def input_update_data(flat_attribute_chair, flat_with_relations):
    data = {
        "flatId": flat_with_relations.id,
        "data": {
            "type": "HOUSE",
            "price": 500.0,
            "roomCount": 2,
            "location": {"country": "US", "city": "Boston"},
            "rooms": [
                {
                    "id": flat_with_relations.rooms.get(type=TypeOfFlatRoom.KITCHEN).id,
                    "type": "KITCHEN",
                    "description": "Updated Description",
                    "attributes": [
                        {
                            "attrId": flat_attribute_chair.id,
                            "count": 20,
                            "description": "Updated Description",
                        }
                    ],
                }
            ],
        },
    }
    return data


@pytest.mark.django_db
class TestFlat:
    def test_create_flat_success(self, client_query, owner, input_create_data):
        credentials = user_credentials(owner)

        response = client_query(
            """
            mutation createFlat($input: FlatCreateInput!) {
                createFlat(data: $input) {
                    ok
                    flat {
                        type
                        price
                        roomCount
                        location {
                            country {
                                name
                                code
                            }
                            city
                        }
                        rooms {
                            type
                            description
                            attributeValues {
                                flatAttribute {
                                    name
                                }
                                count
                                description
                            }
                        }
                    }
                }
            }
            """,
            headers=credentials,
            input_data=input_create_data,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content
        assert content["data"] == {
            "createFlat": {
                "flat": {
                    "type": "HOUSE",
                    "price": 400.2,
                    "roomCount": 3,
                    "location": {
                        "country": {"name": "United States of America", "code": "US"},
                        "city": "New York",
                    },
                    "rooms": [
                        {
                            "type": "KITCHEN",
                            "description": "Description",
                            "attributeValues": [
                                {
                                    "flatAttribute": {"name": "Chair"},
                                    "count": 2,
                                    "description": "Description",
                                },
                                {
                                    "flatAttribute": {"name": "Table"},
                                    "count": 2,
                                    "description": "Description",
                                },
                            ],
                        },
                        {
                            "type": "GUEST_ROOM",
                            "description": "Description",
                            "attributeValues": [
                                {
                                    "flatAttribute": {"name": "Double bad"},
                                    "count": 1,
                                    "description": "Description",
                                }
                            ],
                        },
                    ],
                },
                "ok": True,
            }
        }
        flat = Flat.objects.filter(
            type=TypeOfFlat.HOUSE,
            price=400.2,
            room_count=3,
            location__country="US",
            location__city="New York",
        ).prefetch_related("rooms__attribute_values")
        assert flat.exists()

        flat = flat.first()
        assert flat.rooms.count() == 2
        assert flat.rooms.all()[0].attribute_values.count() == 2
        assert flat.rooms.all()[1].attribute_values.count() == 1

        # check that FlatRoomAttributeValue with flat and attribute instance was created
        for room in input_create_data["rooms"]:
            for attr_value in room["attributes"]:
                attr_id = attr_value["attrId"]
                assert FlatRoomAttributeValue.objects.filter(
                    flat_room__flat=flat, flat_attribute_id=attr_id
                )

    def test_create_flat_for_not_authenticated_user_should_fail(
        self, client_query, input_create_data
    ):
        response = client_query(
            """
            mutation createFlat($input: FlatCreateInput!) {
                createFlat(data: $input) {
                    ok
                    flat {
                        type
                        price
                        roomCount
                        location {
                            country {
                                name
                                code
                            }
                            city
                        }
                        rooms {
                            type
                            description
                            attributeValues {
                                flatAttribute {
                                    name
                                }
                                count
                                description
                            }
                        }
                    }
                }
            }
            """,
            input_data=input_create_data,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        # assert False, content
        assert "errors" in content
        assert (
            content["errors"][0]["message"] == "You do not have permission to perform this action"
        )

        flat = Flat.objects.filter(
            type=TypeOfFlat.HOUSE,
            price=400.2,
            room_count=3,
            location__country="US",
            location__city="New York",
        )
        assert not flat.exists()

    def test_create_flat_when_user_is_not_owner_should_fail(
        self, client_query, renter, input_create_data
    ):
        credentials = user_credentials(renter)

        response = client_query(
            """
            mutation createFlat($input: FlatCreateInput!) {
                createFlat(data: $input) {
                    ok
                    flat {
                        type
                        price
                        roomCount
                        location {
                            country {
                                name
                                code
                            }
                            city
                        }
                        rooms {
                            type
                            description
                            attributeValues {
                                flatAttribute {
                                    name
                                }
                                count
                                description
                            }
                        }
                    }
                }
            }
            """,
            headers=credentials,
            input_data=input_create_data,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" in content
        assert (
            content["errors"][0]["message"] == "You do not have permission to perform this action"
        )

        flat = Flat.objects.filter(
            type=TypeOfFlat.HOUSE,
            price=400.2,
            room_count=3,
            location__country="US",
            location__city="New York",
        )
        assert not flat.exists()

    def test_update_flat_success(self, client_query, owner, input_update_data):
        credentials = user_credentials(owner)

        response = client_query(
            """
            mutation updateFlat($data: FlatUpdateInput!, $flatId: ID!) {
                updateFlat(data: $data, flatId: $flatId) {
                    __typename
                    ... on UpdateFlatSuccess {
                        ok
                        flat {
                            type
                            price
                            roomCount
                            location {
                                country {
                                    name
                                    code
                                }
                                city
                            }
                            rooms {
                                type
                                description
                                attributeValues {
                                    flatAttribute {
                                        name
                                    }
                                    count
                                    description
                                }
                            }
                        }
                    }
                    ... on UpdateFlatFail {
                        ok
                        errorMessage
                    }
                }
            }
            """,
            headers=credentials,
            variables=input_update_data,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" not in content
        assert content["data"] == {
            "updateFlat": {
                "__typename": "UpdateFlatSuccess",
                "flat": {
                    "type": "HOUSE",
                    "price": 500.0,
                    "roomCount": 2,
                    "location": {
                        "country": {"name": "United States of America", "code": "US"},
                        "city": "Boston",
                    },
                    "rooms": [
                        {
                            "type": "KITCHEN",
                            "description": "Updated Description",
                            "attributeValues": [
                                {
                                    "flatAttribute": {"name": "Chair"},
                                    "count": 20,
                                    "description": "Updated Description",
                                }
                            ],
                        },
                        {
                            "type": "GUEST_ROOM",
                            "description": "Description",
                            "attributeValues": [
                                {
                                    "flatAttribute": {"name": "Double bad"},
                                    "count": 1,
                                    "description": "Description",
                                }
                            ],
                        },
                    ],
                },
                "ok": True,
            }
        }
        flat = Flat.objects.filter(
            type=TypeOfFlat.HOUSE,
            price=500.0,
            room_count=2,
            location__country="US",
            location__city="Boston",
        ).prefetch_related("rooms__attribute_values")
        assert flat.exists()

        flat = flat.first()
        assert flat.rooms.count() == 2  # still two rooms despite only 1 was given for updating
        # because only 1 attribute was provided (before changes was 2)
        assert flat.rooms.all()[0].attribute_values.count() == 1
        assert flat.rooms.all()[1].attribute_values.count() == 1

        # check that FlatRoomAttributeValue with flat and attribute instance was created
        for room in input_update_data["data"]["rooms"]:
            for attr_value in room["attributes"]:
                attr_id = attr_value["attrId"]
                assert FlatRoomAttributeValue.objects.filter(
                    flat_room__flat=flat, flat_attribute_id=attr_id
                )

    def test_update_flat_when_user_is_not_owner_should_fail(
        self, client_query, renter, input_update_data
    ):
        credentials = user_credentials(renter)

        response = client_query(
            """
            mutation updateFlat($data: FlatUpdateInput!, $flatId: ID!) {
                updateFlat(data: $data, flatId: $flatId) {
                    __typename
                    ... on UpdateFlatSuccess {
                        ok
                        flat {
                            type
                            price
                            roomCount
                            location {
                                country {
                                    name
                                    code
                                }
                                city
                            }
                            rooms {
                                type
                                description
                                attributeValues {
                                    flatAttribute {
                                        name
                                    }
                                    count
                                    description
                                }
                            }
                        }
                    }
                    ... on UpdateFlatFail {
                        ok
                        errorMessage
                    }
                }
            }
            """,
            headers=credentials,
            variables=input_update_data,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert "errors" in content
        assert (
            content["errors"][0]["message"] == "You do not have permission to perform this action"
        )
        flat = Flat.objects.filter(
            type=TypeOfFlat.HOUSE,
            price=500.0,
            room_count=2,
            location__country="US",
            location__city="Boston",
        )
        assert not flat.exists()

    def test_update_flat_with_user_that_is_not_flat_owner_should_fail(
        self, client_query, input_update_data
    ):
        new_owner = baker.make("users.Owner")
        credentials = user_credentials(new_owner)

        response = client_query(
            """
            mutation updateFlat($data: FlatUpdateInput!, $flatId: ID!) {
                updateFlat(data: $data, flatId: $flatId) {
                    __typename
                    ... on UpdateFlatSuccess {
                        ok
                        flat {
                            type
                            price
                            roomCount
                            location {
                                country {
                                    name
                                    code
                                }
                                city
                            }
                            rooms {
                                type
                                description
                                attributeValues {
                                    flatAttribute {
                                        name
                                    }
                                    count
                                    description
                                }
                            }
                        }
                    }
                    ... on UpdateFlatFail {
                        ok
                        errorMessage
                    }
                }
            }
            """,
            headers=credentials,
            variables=input_update_data,
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert content["data"] == {
            "updateFlat": {
                "__typename": "UpdateFlatFail",
                "errorMessage": "User 9 does not own an flat",
                "ok": False,
            }
        }
        flat = Flat.objects.filter(
            type=TypeOfFlat.HOUSE,
            price=500.0,
            room_count=2,
            location__country="US",
            location__city="Boston",
        )
        assert not flat.exists()

    def test_update_flat_with_defunct_flat_id_should_fail(
        self, client_query, owner, input_update_data
    ):
        credentials = user_credentials(owner)
        input_update_data = copy.deepcopy(input_update_data)
        input_update_data["flatId"] = "3233232"

        response = client_query(
            """
            mutation updateFlat($data: FlatUpdateInput!, $flatId: ID!) {
                updateFlat(data: $data, flatId: $flatId) {
                    __typename
                    ... on UpdateFlatSuccess {
                        ok
                        flat {
                            type
                            price
                            roomCount
                            location {
                                country {
                                    name
                                    code
                                }
                                city
                            }
                            rooms {
                                type
                                description
                                attributeValues {
                                    flatAttribute {
                                        name
                                    }
                                    count
                                    description
                                }
                            }
                        }
                    }
                    ... on UpdateFlatFail {
                        ok
                        errorMessage
                    }
                }
            }
            """,
            headers=credentials,
            variables=input_update_data,
        )

        content = json.loads(response.content)

        assert response.status_code, 200
        assert content["data"] == {
            "updateFlat": {
                "__typename": "UpdateFlatFail",
                "errorMessage": "User 10 does not own an flat",
                "ok": False,
            }
        }
        flat = Flat.objects.filter(
            type=TypeOfFlat.HOUSE,
            price=500.0,
            room_count=2,
            location__country="US",
            location__city="Boston",
        )
        assert not flat.exists()

    def test_delete_flat_success(self, client_query, owner, flat_with_relations):
        credentials = user_credentials(owner)

        response = client_query(
            """
            mutation deleteFlat($id: ID!) {
                deleteFlat(id: $id) {
                    __typename
                    ... on DeleteFlatSuccess {
                        ok
                    }
                    ... on DeleteFlatFail {
                        ok
                        errorMessage
                    }
                }
            }
            """,
            headers=credentials,
            variables={"id": flat_with_relations.id},
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert content["data"] == {"deleteFlat": {"__typename": "DeleteFlatSuccess", "ok": True}}
        is_flat_exists = Flat.objects.filter(id=flat_with_relations.id).exists()

        assert not is_flat_exists

    def test_delete_flat_with_non_owner_should_fail(self, client_query, owner):
        credentials = user_credentials(owner)
        new_flat = baker.make("flats.Flat")

        response = client_query(
            """
            mutation deleteFlat($id: ID!) {
                deleteFlat(id: $id) {
                    __typename
                    ... on DeleteFlatSuccess {
                        ok
                    }
                    ... on DeleteFlatFail {
                        ok
                        errorMessage
                    }
                }
            }
            """,
            headers=credentials,
            variables={"id": new_flat.id},
        )

        content = json.loads(response.content)
        assert response.status_code, 200
        assert content["data"] == {
            "deleteFlat": {
                "__typename": "DeleteFlatFail",
                "errorMessage": "Flat with id 7 and owner 14 does not exists.",
                "ok": False,
            }
        }
        is_flat_exists = Flat.objects.filter(
            id=new_flat.id,
        ).exists()

        assert is_flat_exists
