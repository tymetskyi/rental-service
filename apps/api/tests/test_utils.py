from api.utils import get_role_for_query
from users.constants import UserRole


def test_get_role_for_query():
    role = get_role_for_query(is_owner=True)
    assert role == UserRole.RENTER

    role = get_role_for_query(is_owner=False)
    assert role == UserRole.OWNER
