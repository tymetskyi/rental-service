import graphene

from flats.constants import TypeOfFlat, TypeOfFlatRoom
from users.constants import UserStatus

UserStatusEnum = graphene.Enum.from_enum(UserStatus)
FlatTypeEnum = graphene.Enum.from_enum(TypeOfFlat)
FlatRoomEnum = graphene.Enum.from_enum(TypeOfFlatRoom)
