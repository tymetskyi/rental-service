from django.db import transaction

from flats.models import Flat, FlatRoom, FlatRoomAttributeValue
from locations.models import Location


@transaction.atomic
def update_flat(
    data,
    flat_id: int,
) -> Flat:
    """Flat updating logic."""
    flat = Flat.objects.get(id=flat_id)
    location_data = data.pop("location")
    rooms = data.pop("rooms")

    Flat.objects.filter(id=flat.id).update(**data)  # update flat
    Location.objects.filter(id=flat.location_id).update(**location_data)  # update flat location

    # update flat rooms
    rooms_data = {int(room.pop("id")): room for room in rooms}
    room_data_ids = rooms_data.keys()

    flat_rooms = FlatRoom.objects.filter(flat_id=flat_id, id__in=room_data_ids)
    for flat_room in flat_rooms:
        flat_room.type = rooms_data[flat_room.id]["type"]
        flat_room.description = rooms_data[flat_room.id]["description"]

    # updated flat rooms
    FlatRoom.objects.bulk_update(flat_rooms, fields=["type", "description"])

    # delete attribute values from submitted room ids and create new
    FlatRoomAttributeValue.objects.filter(
        flat_room__in=room_data_ids, flat_room__flat_id=flat_id
    ).delete()
    FlatRoomAttributeValue.objects.bulk_create(
        [
            FlatRoomAttributeValue(
                flat_attribute_id=attr["attr_id"],
                flat_room_id=room_id,
                description=attr["description"],
                count=attr["count"],
            )
            for room_id, data in rooms_data.items()
            for attr in data["attributes"]
        ]
    )

    flat.refresh_from_db()
    return flat


@transaction.atomic
def create_flat(info, data) -> Flat:
    """Flat creation logic."""
    user_id = info.context.user.id
    location_data = data.pop("location")
    rooms_data = data.pop("rooms")

    location = Location.objects.create(**location_data)  # create location
    flat = Flat.objects.create(location=location, owner_id=user_id, **data)  # create Flat

    # create flat rooms
    for room in rooms_data:
        attributes = room.pop("attributes")
        flat_room = FlatRoom.objects.create(flat=flat, **room)  # plus type and/or description
        if attributes:
            ThroughModel = flat_room.attributes.through
            ThroughModel.objects.bulk_create(
                [
                    ThroughModel(
                        flat_attribute_id=attribute.pop("attr_id"),
                        flat_room=flat_room,
                        **attribute  # plus count and/or description
                    )
                    for attribute in attributes
                ]
            )
    return flat
