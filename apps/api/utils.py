from enum import Enum

from users.constants import UserRole


def get_role_for_query(is_owner: bool) -> Enum:
    """Choice role by is_owner flag."""
    if is_owner:
        return UserRole.RENTER
    return UserRole.OWNER
