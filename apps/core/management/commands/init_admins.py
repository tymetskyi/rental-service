from django.core.management import BaseCommand

from config import settings
from users.models import Owner, User


class Command(BaseCommand):
    def handle(self, *args, **options):
        if User.objects.filter(is_superuser=True, is_staff=True).count() == 0:
            for user in settings.ADMINS:
                first_name, email = user[0], user[1]
                print(f"Creating account for {first_name} with email {email}.")
                Owner.objects.create_superuser(
                    first_name=first_name, email=email, password="password"
                )
        else:
            print("Admins has already exists.")
