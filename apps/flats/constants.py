from django.db import models
from django.utils.translation import gettext_lazy as _


class TypeOfFlat(models.TextChoices):
    STUDIO = "studio", _("Studio")
    HOUSE = "house", _("House")
    PRIVATE_ROOM = (
        "private_room",
        _("Private room"),
    )


class TypeOfFlatRoom(models.TextChoices):
    KITCHEN = "kitchen", _("Kitchen")
    LIVING_ROOM = "living_room", _("Living Room")
    GUEST_ROOM = "guest_room", _("Guest Room")
    BEDROOM = "bedroom", _("Bedroom")
