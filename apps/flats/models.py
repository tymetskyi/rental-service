from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from model_utils.models import TimeStampedModel

from flats.constants import TypeOfFlat, TypeOfFlatRoom


class Flat(TimeStampedModel):
    location = models.ForeignKey(
        "locations.Location",
        on_delete=models.CASCADE,
        related_name="flats",
        verbose_name=_("Location"),
    )
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        related_name="flats",
        verbose_name=_("Owner"),
    )
    room_count = models.PositiveIntegerField(default=1)
    type = models.CharField(_("Flat type"), max_length=50, choices=TypeOfFlat.choices)
    price = models.FloatField(_("Price"))

    class Meta:
        verbose_name = _("Flat")
        verbose_name_plural = _("Flats")

    def __str__(self):
        return f"Flat {self.id}"


class FlatRoom(models.Model):
    flat = models.ForeignKey(
        "flats.Flat",
        on_delete=models.CASCADE,
        related_name="rooms",
        verbose_name=_("Flat"),
    )
    type = models.CharField(_("Flat type"), max_length=50, choices=TypeOfFlatRoom.choices)
    description = models.TextField(_("Description"), blank=True)

    class Meta:
        verbose_name = _("Flat room")
        verbose_name_plural = _("Flat rooms")

    def __str__(self):
        return f"Flat room {self.id}"


class FlatRoomAttribute(models.Model):
    name = models.CharField(_("Name"), max_length=255)
    flat_rooms = models.ManyToManyField(
        "flats.FlatRoom",
        through="flats.FlatRoomAttributeValue",
        related_name="attributes",
    )

    class Meta:
        verbose_name = _("Flat attribute")
        verbose_name_plural = _("Flat attributes")

    def __str__(self):
        return f"{self.id}, {self.name}"


class FlatRoomAttributeValue(models.Model):
    flat_attribute = models.ForeignKey(
        "flats.FlatRoomAttribute",
        on_delete=models.CASCADE,
        related_name="attribute_values",
        verbose_name=_("Flat attribute"),
    )
    flat_room = models.ForeignKey(
        "flats.FlatRoom",
        on_delete=models.CASCADE,
        related_name="attribute_values",
        verbose_name=_("Flat room"),
    )
    count = models.PositiveIntegerField(default=1)
    description = models.TextField(_("Description"), blank=True)

    class Meta:
        verbose_name = _("Flat attribute value")
        verbose_name_plural = _("Flat attribute values")

    def __str__(self):
        return (
            f"Through model between flat attribute:"
            f" {self.flat_attribute_id} and flat room {self.flat_room_id}"
        )
