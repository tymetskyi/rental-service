from django.contrib import admin

from flats.models import Flat, FlatRoom, FlatRoomAttribute, FlatRoomAttributeValue


class FlatRoomAttributeValueInline(admin.TabularInline):
    """FlatRoomAttributeValue inline admin model."""

    model = FlatRoomAttributeValue
    extra = 1


@admin.register(Flat)
class FlatAdmin(admin.ModelAdmin):
    """Flat admin model."""

    list_display = [field.name for field in Flat._meta.fields]


@admin.register(FlatRoom)
class FlatRoomAdmin(admin.ModelAdmin):
    """FlatRoom admin model."""

    list_display = [field.name for field in FlatRoom._meta.fields]
    inlines = [FlatRoomAttributeValueInline]


@admin.register(FlatRoomAttribute)
class FlatAttributeAdmin(admin.ModelAdmin):
    """FlatAttribute admin model."""

    list_display = [field.name for field in FlatRoomAttribute._meta.fields]
