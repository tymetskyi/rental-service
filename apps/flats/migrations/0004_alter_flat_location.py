# Generated by Django 3.2.10 on 2021-12-09 18:22

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('locations', '0001_initial'),
        ('flats', '0003_auto_20211209_1810'),
    ]

    operations = [
        migrations.AlterField(
            model_name='flat',
            name='location',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='flats', to='locations.location', verbose_name='Location'),
        ),
    ]
