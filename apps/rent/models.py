from django.conf import settings
from django.db import models
from django.utils.translation import gettext_lazy as _

from model_utils.models import TimeStampedModel


class RentContract(TimeStampedModel):
    renter = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name=_("Renter"),
        related_name="contracts",
    )
    flat = models.ForeignKey(
        "flats.Flat",
        on_delete=models.CASCADE,
        verbose_name=_("Flat"),
        related_name="rent_contracts",
    )
    description = models.TextField(_("Description"), blank=True)

    class Meta:
        verbose_name = _("Rent contract")
        verbose_name_plural = _("Rent contracts")

    def __str__(self):
        return f"Contract {self.id}, on flat {self.flat_id}"


class MonthlyPayment(TimeStampedModel):
    renter_contract = models.ForeignKey(
        "rent.RentContract",
        on_delete=models.CASCADE,
        verbose_name=_("Rent contract"),
        related_name="monthly_payments",
    )
    is_payed = models.BooleanField(default=False)
    date = models.DateField(null=True, blank=True)

    class Meta:
        verbose_name = _("Monthly payment")
        verbose_name_plural = _("Monthly payments")

    def __str__(self):
        return f"Monthly payment {self.id}"

    @property
    def payed_on_month(self) -> str:
        return self.date.strftime("%B")
