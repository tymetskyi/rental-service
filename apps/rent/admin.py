from django.contrib import admin

from rent.models import MonthlyPayment, RentContract


class MonthlyPaymentInline(admin.TabularInline):
    """MonthlyPayment admin inline model."""

    model = MonthlyPayment
    extra = 1
    readonly_fields = ["payed_on_month"]

    def payed_on_month(self, obj) -> str:
        """Custom field based on model property."""
        return obj.payed_on_month


@admin.register(RentContract)
class RentContractAdmin(admin.ModelAdmin):
    """RentContract admin model."""

    list_display = [field.name for field in RentContract._meta.fields]
    inlines = [MonthlyPaymentInline]
