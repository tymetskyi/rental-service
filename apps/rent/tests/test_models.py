from datetime import datetime

import pytest
from model_bakery import baker


@pytest.mark.django_db
def test_payed_on_month():
    monthly_payment = baker.make("rent.MonthlyPayment", date=datetime(2020, 1, 1))
    expected = "January"
    assert monthly_payment.payed_on_month == expected
