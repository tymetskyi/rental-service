from django.contrib.auth.forms import UserCreationForm

from users.models import Owner, Renter, User


class BaseUserForm(UserCreationForm):
    """Base form for User model."""

    class Meta:
        model = User
        fields = (
            "id",
            "password1",
            "password2",
            "first_name",
            "last_name",
            "email",
            "address",
            "phone_number",
        )


class OwnerForm(BaseUserForm):
    """Owner form based on BaseUserForm."""

    class Meta(BaseUserForm.Meta):
        model = Owner


class RenterForm(BaseUserForm):
    """Renter form based on BaseUserForm."""

    class Meta(BaseUserForm.Meta):
        model = Renter
