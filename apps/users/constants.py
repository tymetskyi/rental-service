from django.db import models
from django.utils.translation import gettext_lazy as _


class UserRole(models.TextChoices):
    OWNER = "owner", _("Owner")
    RENTER = "renter", _("Renter")


class UserStatus(models.TextChoices):
    ACTIVE = "active", _("Active")
    INACTIVE = "inactive", _("Inactive")
