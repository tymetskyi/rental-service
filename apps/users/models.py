from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _

from model_utils.models import TimeStampedModel

from users.constants import UserRole, UserStatus
from users.managers import CustomUserManager, OwnerManager, RenterManager


class User(AbstractUser, TimeStampedModel):
    username = None
    email = models.EmailField(
        _("Email address"),
        unique=True,
        error_messages={"unique": _("User with this email address already exists.")},
    )
    birthday = models.DateField(_("Birthday"), null=True, blank=True)
    role = models.CharField(_("Role"), max_length=10, choices=UserRole.choices, blank=True)
    status = models.CharField(
        _("Status"),
        max_length=10,
        choices=UserStatus.choices,
        default=UserStatus.ACTIVE,
    )
    phone_number = models.CharField(_("Phone number"), max_length=17, blank=True)
    address = models.CharField(_("Address"), max_length=200, blank=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()
    base_role = None

    class Meta:
        db_table = "custom_user"

    def __str__(self):
        return f"{self.id}, {self.email}"

    def save(self, *args, **kwargs):
        if not self.pk and self.base_role:
            self.role = self.base_role
        super().save(*args, **kwargs)

    @property
    def is_owner(self) -> bool:
        return self.role == UserRole.OWNER

    @property
    def full_name(self) -> str:
        full_name = self.get_full_name()
        return full_name.title()

    def set_account_status(self, status) -> None:
        if self.status != status:
            self.status = status
            self.save(update_fields=["status"])


class Owner(User):
    objects = OwnerManager()
    base_role = UserRole.OWNER

    class Meta:
        proxy = True


class Renter(User):
    objects = RenterManager()
    base_role = UserRole.RENTER

    class Meta:
        proxy = True
