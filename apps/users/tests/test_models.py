import pytest
from model_bakery import baker

from users.constants import UserRole, UserStatus


@pytest.fixture()
def user():
    return baker.make("users.User", first_name="Jon", last_name="Dou")


@pytest.mark.django_db
class TestUser:
    """Test user model."""

    def test_user_full_name(self, user):
        assert user.full_name == "Jon Dou"

    def test_set_account_status(self, user):
        assert user.status == UserStatus.ACTIVE

        user.set_account_status(UserStatus.INACTIVE)
        assert user.status == UserStatus.INACTIVE

    def test_owner_creation(self):
        owner = baker.make("users.Owner")
        assert owner.role == UserRole.OWNER

    def test_renter_creation(self):
        renter = baker.make("users.Renter")
        assert renter.role == UserRole.RENTER
