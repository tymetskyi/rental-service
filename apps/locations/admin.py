from django.contrib import admin

from locations.models import Location


@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    """Location admin model."""

    list_display = [field.name for field in Location._meta.fields]
