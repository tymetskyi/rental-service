from django.db import models
from django.utils.translation import gettext_lazy as _

from django_countries.fields import CountryField


class Location(models.Model):
    country = CountryField(_("Country"))
    city = models.CharField(_("City"), max_length=255)

    class Meta:
        verbose_name = _("Location")
        verbose_name_plural = _("Locations")

    def __str__(self):
        return f"{self.id}, {self.country.name}, {self.city}"
