FROM python:3.9
WORKDIR /code

# set env variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update && apt-get -y install postgresql-client && rm -rf /var/lib/apt/lists

# copy requirements file in app
COPY ./requirements requirements

# isntall requirements
RUN pip install --upgrade pip && pip install --no-cache-dir -r requirements/local.txt

# copy project
COPY . /code/

CMD python manage.py migrate && python manage.py init_admins && python manage.py runserver 0.0.0.0:8000
