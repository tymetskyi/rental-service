## The Rental Service project is a server-side implementation of the GraphQL spec using Django + [Graphene](https://docs.graphene-python.org/projects/django/en/latest/)
<br>

### Getting started

1. Create .env file from .env.example

2. Build and run an image: 
> make start

3. Superuser will be created automatically from ADMIN_USERS
4. Migrations will be applied automatically likewise
5. Open app on localhost:8000


### Install git pre-commit hooks with the command:

`$ venv/bin/pre-commit install`

or just

`$ pre-commit install`

After that every time you commit the code, some tasks will be launched. For instance: black, isort, flake8, migrations checking and other.

___

### Testing & linters

```
# Run tests:
make test

# Check linters
make check_linters

# Run linters
make run_linters
```

### Another useful commands
> make
