import graphene
import graphql_jwt

import api.mutations as app_mutations
import api.queries as app_queries


class Query(app_queries.Query, graphene.ObjectType):
    pass


class Mutation(
    app_mutations.Mutation,
    graphene.ObjectType,
):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    verify_token = graphql_jwt.Verify.Field()


schema = graphene.Schema(query=Query, mutation=Mutation)
